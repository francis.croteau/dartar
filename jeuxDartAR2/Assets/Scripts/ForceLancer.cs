﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForceLancer : MonoBehaviour
{
    // Start is called before the first frame update

    private Slider slider;
    public float valeurCible = 0.4f;
    private bool augmenter = true;


    public void Awake()
    {
        slider = gameObject.GetComponent<Slider>();
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (valeurCible <= 0.7f & augmenter)
        {
            valeurCible = valeurCible + 0.001f;
            if (valeurCible < 0.7f)
            {
                augmenter = true;
            }
            else
            {
                augmenter = false;
            }
        }
        else if (valeurCible >= 0.4f)
        {
            valeurCible = valeurCible - 0.001f;
            if (valeurCible == 0.4f | valeurCible < 0.4f)
            {
                augmenter = true;
            }
        }
        slider.value = valeurCible;
    }

}