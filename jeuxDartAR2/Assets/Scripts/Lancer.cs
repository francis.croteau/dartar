﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lancer : MonoBehaviour
{
    public float puissance = 0.5f;
    public Rigidbody rb;
    public GameObject Slider;
    public ForceLancer forceControl;
    public GameObject txt_score;
    public score scoreControler;
    public Vector3 positionOriginale;
    public bool est_lancer;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
        rb.isKinematic = true;
        Slider = GameObject.Find("Niveau Force");
        forceControl = Slider.GetComponent<ForceLancer>();
        txt_score = GameObject.Find("Txt_score");
        scoreControler = txt_score.GetComponent<score>();
        positionOriginale = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
        est_lancer = false;


    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LancerDart()
    {
        if (!est_lancer)
        {
            puissance = forceControl.valeurCible;
            rb.useGravity = true;
            rb.isKinematic = false;
            rb.AddForce(-puissance*7.6f, puissance*7.2f, 0, ForceMode.Impulse);
        }
    }

    public void OnCollisionEnter(Collision col)
    {
        rb.useGravity = false;
        rb.isKinematic = true;
        if (col.gameObject.name == "cible_100")
        {
            scoreControler.Ajouter_100();
        }
        else if (col.gameObject.name == "cible_25")
        {
            scoreControler.Ajouter_25();
        }
        else if (col.gameObject.name == "cible_10")
        {
            scoreControler.Ajouter_10();
        }
    }

    public void ReplacerDart()
    {
        gameObject.transform.position = positionOriginale;
    }
}
