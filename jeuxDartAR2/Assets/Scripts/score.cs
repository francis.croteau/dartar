﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class score : MonoBehaviour
{

    public int points;
    Text valeurText;
    // Start is called before the first frame update
    void Start()
    {
        valeurText = GetComponent<Text>();
        points = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Ajouter_100()
    {
        points += 100;
        valeurText.text = points.ToString();
    }

    public void Ajouter_25()
    {
        points += 25;
        valeurText.text = points.ToString();
    }

    public void Ajouter_10()
    {
        points += 10;
        valeurText.text = points.ToString();
    }



}
